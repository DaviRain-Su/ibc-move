// SPDX-License-Identifier: Apache-2.0

module ibc_move::Identifier {

    use std::string;
    use aptos_std::aptos_hash::keccak256;
    use std::vector;

    // constant values
    const COMMITMENT_SLOT: u64 = 0;

    // commitment key generator

    public fun client_commitment_key(client_id: string::String): vector<u8> {
        let encode_value: string::String = string::utf8(b"");

        // push 'clients'
        string::append(&mut encode_value, string::utf8(b"clients/"));

        // push client_id
        string::append(&mut encode_value, client_id);

        // push clientstate
        string::append(&mut encode_value, string::utf8(b"/clientState"));

        keccak256(*string::bytes(&encode_value))
    }

    public fun consensus_commitment_key(
        client_id: string::String,
        revision_number: u64,
        revision_height: u64,
    ): vector<u8> {
        let encode_value: string::String = string::utf8(b"");

        // push 'clients'
        string::append(&mut encode_value, string::utf8(b"clients/"));

        // push client_id
        string::append(&mut encode_value, client_id);

        // push consensusStates
        string::append(&mut encode_value, string::utf8(b"/consensusStates/"));

        // push revision_number
        string::append(&mut encode_value, uint2str(revision_number));

        // push "-"
        string::append(&mut encode_value, string::utf8(b"-"));

        // push revision_height
        string::append(&mut encode_value, uint2str(revision_height));

        keccak256(*string::bytes(&encode_value))
    }

    public fun connection_commitment_key(
        connection_id: string::String,
    ): vector<u8> {
        let encode_value: string::String = string::utf8(b"");

        // push "connections/"
        string::append(&mut encode_value, string::utf8(b"connections/"));

        // push connection_id
        string::append(&mut encode_value, connection_id);

        keccak256(*string::bytes(&encode_value))
    }

    public fun channel_commitment_key(
        port_id: string::String,
        channel_id: string::String,
    ): vector<u8> {
        let encode_value: string::String = string::utf8(b"");

        // push "channelEnds/ports/"
        string::append(&mut encode_value, string::utf8(b"channelEnds/ports/"));

        // push "portid"
        string::append(&mut encode_value, port_id);

        // push "channels"
        string::append(&mut encode_value, string::utf8(b"/channels/"));

        // push channelid
        string::append(&mut encode_value, channel_id);

        keccak256(*string::bytes(&encode_value))
    }

    public fun packet_commitment_key(
        port_id: string::String,
        channel_id: string::String,
        sequence: u64
    ): vector<u8> {

        let encode_value: string::String = string::utf8(b"");

        // push "commitments/ports/"
        string::append(&mut encode_value, string::utf8(b"commitments/ports/"));

        // push "portid"
        string::append(&mut encode_value, port_id);

        // push "channels"
        string::append(&mut encode_value, string::utf8(b"/channels/"));

        // push channelid
        string::append(&mut encode_value, channel_id);

        // push "sequences"
        string::append(&mut encode_value, string::utf8(b"/sequences/"));

        // push sequence
        string::append(&mut encode_value, uint2str(sequence));

        keccak256(*string::bytes(&encode_value))
    }

    public fun packet_acknowledgement_commitment_key(
        port_id: string::String,
        channel_id: string::String,
        sequence: u64,
    ): vector<u8> {

        let encode_value: string::String = string::utf8(b"");

        // push "acks/ports/"
        string::append(&mut encode_value, string::utf8(b"acks/ports/"));

        // push "portid"
        string::append(&mut encode_value, port_id);

        // push "channels"
        string::append(&mut encode_value, string::utf8(b"/channels/"));

        // push channelid
        string::append(&mut encode_value, channel_id);

        // push "sequences"
        string::append(&mut encode_value, string::utf8(b"/sequences/"));

        // push sequence
        string::append(&mut encode_value, uint2str(sequence));

        keccak256(*string::bytes(&encode_value))
    }

    public fun packet_receipt_commitment_key(
        port_id: string::String,
        channel_id: string::String,
        sequence: u64,
    ): vector<u8> {


        let encode_value: string::String = string::utf8(b"");

        // push "receipts/ports/"
        string::append(&mut encode_value, string::utf8(b"receipts/ports/"));

        // push "portid"
        string::append(&mut encode_value, port_id);

        // push "channels"
        string::append(&mut encode_value, string::utf8(b"/channels/"));

        // push channelid
        string::append(&mut encode_value, channel_id);

        // push "sequences"
        string::append(&mut encode_value, string::utf8(b"/sequences/"));

        // push sequence
        string::append(&mut encode_value, uint2str(sequence));

        keccak256(*string::bytes(&encode_value))
    }

    public fun next_sequence_recv_commitment_key(
        port_id: string::String,
        channel_id: string::String,
    ): vector<u8> {

        let encode_value: string::String = string::utf8(b"");

        // push "nextsequenceRecv/ports/"
        string::append(&mut encode_value, string::utf8(b"nextsequenceRecv/ports/"));

        // push "portid"
        string::append(&mut encode_value, port_id);

        // push "channels"
        string::append(&mut encode_value, string::utf8(b"/channels/"));

        // push channelid
        string::append(&mut encode_value, channel_id);

        keccak256(*string::bytes(&encode_value))
    }

    // slot calcuator
    public fun  client_state_commitment_slot(client_id: string::String): vector<u8> {
        let encode_value: vector<u8> = vector[];

        vector::append(&mut encode_value, client_commitment_key(client_id));

        vector::append(&mut encode_value, *string::bytes(&uint2str(COMMITMENT_SLOT)));

        keccak256(encode_value)
    }

    public fun consensus_state_commitment_slot(
        client_id: string::String,
        revision_number: u64,
        revision_height: u64,
    ): vector<u8> {
        let encode_value: vector<u8> = vector[];

        vector::append(&mut encode_value, consensus_commitment_key(client_id, revision_number,revision_height));

        vector::append(&mut encode_value, *string::bytes(&uint2str(COMMITMENT_SLOT)));

        keccak256(encode_value)
    }

    public fun connection_commitment_slot(connection_id: string::String): vector<u8>{
        let encode_value: vector<u8> = vector[];

        vector::append(&mut encode_value, connection_commitment_key(connection_id));

        vector::append(&mut encode_value, *string::bytes(&uint2str(COMMITMENT_SLOT)));

        keccak256(encode_value)
    }

    public fun channel_commitment_slot(port_id: string::String, channel_id: string::String): vector<u8> {
        let encode_value: vector<u8> = vector[];

        vector::append(&mut encode_value, channel_commitment_key(port_id, channel_id));

        vector::append(&mut encode_value, *string::bytes(&uint2str(COMMITMENT_SLOT)));

        keccak256(encode_value)
    }

    public fun packet_commitment_slot(port_id: string::String, channel_id: string::String, sequence: u64): vector<u8> {
        let encode_value: vector<u8> = vector[];

        vector::append(&mut encode_value, packet_commitment_key(port_id, channel_id, sequence));

        vector::append(&mut encode_value, *string::bytes(&uint2str(COMMITMENT_SLOT)));

        keccak256(encode_value)
    }

    public fun packet_acknowledgement_commitment_slot(port_id: string::String, channel_id: string::String, sequence: u64) : vector<u8> {
        let encode_value: vector<u8> = vector[];

        vector::append(&mut encode_value, packet_acknowledgement_commitment_key(port_id, channel_id, sequence));

        vector::append(&mut encode_value, *string::bytes(&uint2str(COMMITMENT_SLOT)));

        keccak256(encode_value)
    }

    public fun packet_receipt_commitment_slot(port_id: string::String, channel_id: string::String, sequence: u64): vector<u8> {
        let encode_value: vector<u8> = vector[];

        vector::append(&mut encode_value, packet_receipt_commitment_key(port_id, channel_id, sequence));

        vector::append(&mut encode_value, *string::bytes(&uint2str(COMMITMENT_SLOT)));

        keccak256(encode_value)
    }

    public fun next_sequence_recv_commitment_slot(port_id: string::String, channel_id: string::String) : vector<u8> {
        let encode_value: vector<u8> = vector[];

        vector::append(&mut encode_value, next_sequence_recv_commitment_key(port_id, channel_id));

        vector::append(&mut encode_value, *string::bytes(&uint2str(COMMITMENT_SLOT)));

        keccak256(encode_value)
    }

    // capability path

    public fun port_capability_path(port_id: string::String): vector<u8> {
        *string::bytes(&port_id)
    }

    public fun channel_capability_path(port_id: string::String, channel_id: string::String): vector<u8> {
        let result: vector<u8> = vector[];
        vector::append(&mut result, *string::bytes(&port_id));
        vector::append(&mut result, b"/");
        vector::append(&mut result, *string::bytes(&channel_id));
        result
    }

    // utility functions

    fun uint2str(_i: u64): string::String {
        if (_i == 0) {
            return string::utf8(b"0")
        };

        let j = _i;
        let len: u64 = 0;
        while (j != 0) {
            len = len + 1;
            j = j / 10;
        };

        let bstr: vector<u8> = vector[];
        let k = len;

        while (_i != 0) {
            k = k - 1;
            let temp = (48 + (_i - _i / 10 * 10) as u8);
             *vector::borrow_mut(&mut bstr, k) = temp;
            _i = _i / 10;
        };

        return string::utf8(bstr)
    }

}   