// SPDX-License-Identifier: Apache-2.0

// https://github.com/celestiaorg/protobuf3-solidity-lib/blob/master/contracts/ProtobufLib.sol

/**
* @title Runtime library for ProtoBuf serialization and/or deserialization.
* All ProtoBuf generated code will use this library.
*/
module ibc_move::ProtoBufRuntime {
    // @notice ProtoBuf wire type.

    const WrieTypeVarint: u64 = 0;
    const WriteTypeFixed64: u64 = 1;
    const WriteTypeLengthDelim: u64 = 2;
    const WriteTypeStartGroup: u64 = 3;
    const WriteTypeEndGroup: u64 = 4;
    const WriteTypeFixed32: u64 = 5;

    /// @dev Maximum number of bytes for a varint.
    /// @dev 64 bit, in groups of base-128(7 bits).
    const MAX_VARINT_SIZE: u64 = 10;



    ////////////////////////////////////
    // Decoding
    ////////////////////////////////////

    /// @notice Decode key.
    /// @dev https://developers.google.com/protocol-buffers/docs/encoding#structure
    /// @param p Position
    /// @param buf Buffer
    /// @return Success
    /// @return New position
    /// @return Field number
    /// @return Wire type
    fun decode_key(position: u64, buf: vector<u8>): (bool, u64, u64, u64) {
        todo!()
    }

    /// @notice Decode varint.
    /// @dev https://developers.google.com/protocol-buffers/docs/encoding#varints
    /// @param p Position
    /// @param buf Buffer
    /// @return Success
    /// @return New position
    /// @return Decoded int
    fun decode_varint(p: u64, buf: vector<u8>): (bool, u64, u64) {
        todo!()
    }

    // @notice Decode varint int32.
    // @param p Position
    // @param buf Buffer
    // @return Success
    // @return New position
    // @return Decoded int
    // fun decode_int32(p: u64, buf: vector<u8>): (bool, u64, i32) {
    //     todo!()
    // }

    // @notice Decode varint int64.
    // @param p Position
    // @param buf Buffer
    // @return Success
    // @return New position
    // @return Decoded int
    // fun decode_int64(p: u64, buf: vector<u8>): (bool, u64, i64) {
    //     todo!()
    // }
}